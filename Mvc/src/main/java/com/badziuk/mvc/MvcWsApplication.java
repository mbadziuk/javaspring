package com.badziuk.mvc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MvcWsApplication {

	public static void main(String[] args) {
		SpringApplication.run(MvcWsApplication.class, args);
	}
}
