package com.badziuk.mvc;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.net.URI;
import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/api")
public class HelloController {

    @GetMapping(path = "/persons")
    public List<Person> getPersons() {
        return Arrays.asList(new Person("Maksym", 23, 44),new Person("Denis", 2 + 130, 3 -15));
    }

    @PostMapping(path = "/persons/{id}")
    public ResponseEntity updatePerson(@Valid @RequestBody Person person, @PathVariable int id) {

        if(id == 11) {
            throw new PersonNotFoundException("Person wasn't found for Updating! " + id);
        }

        System.out.println("Updated: " +person);

        return ResponseEntity.accepted().build();
    }

    @GetMapping(path = "/persons/{personId}")
    public Person getPerson(@PathVariable int personId) {

        if(personId == 66) {
            throw new PersonNotFoundException("Person wasn't found! " + personId);
        }
        return new Person("Maksym", personId, personId);
    }

    @PutMapping(path = "/persons/{id}")
    public ResponseEntity createPerson(@Valid @RequestBody Person person, @PathVariable int id) {

        System.out.println("Created: " + person);

        return ResponseEntity.created(URI.create("/persons/"+id)).build();
    }

    @DeleteMapping(path = "/persons/{id}")
    public ResponseEntity deleteUser(@PathVariable int id) {

        System.out.println("User deleted. ID = " + id);

        return ResponseEntity.ok().build();
    }



}
