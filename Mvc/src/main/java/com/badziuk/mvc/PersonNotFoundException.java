package com.badziuk.mvc;

public class PersonNotFoundException extends RuntimeException {

    private String message;

   public PersonNotFoundException(String message){
       this.message=message;
   }

   public PersonNotFoundException(){}

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
